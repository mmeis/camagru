
FROM debian:latest

LABEL desc="nginx-php-fpm-minimal"
LABEL maintainer="mmeisson@student.42.fr"

RUN apt update 
RUN apt install php7.0 php7.0-fpm php7.0-pgsql nginx -y

RUN echo "\ninclude=/etc/php/7.0/fpm/php-fpm-add.d/*.conf" > /etc/php/7.0/fpm/php-fpm.conf && \
	mkdir -p 	/var/php-fpm/log \
				/var/run/ \
				/usr/local/etc/php-fpm-add.d && \
	touch		/var/php-fpm/log/error.log \
				/var/php-fpm/log/access.log \
				/var/php-fpm/log/slow.log


CMD service nginx start && php-fpm7.0 -F -c php.ini
