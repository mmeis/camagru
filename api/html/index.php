<?php

if ($_SERVER["CONTENT_TYPE"] == "application/json") {
	$_POST = json_decode(file_get_contents('php://input'), true);
}

require_once("framework/route.php");

$v1Api = new Route();
$router = new Route();

$routeV1 = Array(
	"|^/user/?$|" => "user",
	"|^/connect/?$|" => "connect",
	"|^/user/?([a-zA-Z0-9-_]+)?/?$|" => "user",
);
$v1Api->register($routeV1);


$route = Array(
	"|^/api/v1(.*)|" => $v1Api,
	"|^/install/?$|" => "install",
);
$router->register($route);
$router->process(strtok($_SERVER["REQUEST_URI"],'?'));

?>
