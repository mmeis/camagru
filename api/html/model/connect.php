<?php

require_once("settings.php");

function connect(string $login, string $password) {
	$request = <<<EOT
INSERT INTO
		sessions (user_id)
(
	SELECT
		id
	FROM
		users
	WHERE
		login = :login AND password = :password
)
RETURNING id
EOT;

	$bdd = DataBase::getInstance();

	$prep = $bdd->prepare($request);
	$prep->bindParam(":login", $login, PDO::PARAM_STR);
	$prep->bindParam(":password", $password, PDO::PARAM_STR);

	$prep->execute();
	$result = $prep->fetch();
	$prep->closeCursor();

	return $result;
}

function disconnect(int $session_id) {
	$request = <<<EOT
UPDATE
	sessions
SET
	is_connected = false
WHERE
	id = :session_id
RETURNING 1
EOT;

	$bdd = DataBase::getInstance();

	$prep = $bdd->prepare($request);
	$prep->bindParam(":session_id", $session_id, PDO::PARAM_INT);

	$prep->execute();
	return ($prep->fetchAll());
}
