<?php

require_once("settings.php");

function install() {
		$sql_install = "tables.sql";

		if (file_exists($sql_install) === false) {
				die("The file to install bdd does not exists :'(");
		}
		$file_content = file_get_contents($sql_install);
		$bdd = DataBase::getInstance();

		return ($bdd->exec($file_content) !== false);
}

?>
