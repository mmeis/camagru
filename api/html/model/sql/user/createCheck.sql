
SELECT
	sum(CASE WHEN pseudo = :pseudo THEN 1 ELSE 0 end) AS pseudo_exists,
	sum(CASE WHEN login = :login THEN 1 ELSE 0 end) AS login_exists,
	sum(CASE WHEN email = :email THEN 1 ELSE 0 end) AS email_exists
FROM
	users
