<?php

require_once("settings.php");

/*

	AND validation.login_valid <> FALSE AND
	validation.email_valid <> FALSE

 */
function create_check_error(string $login, string $pseudo, string $email) {
	/*
	 *	Create a user with a complete set of data
	 *
	 *	Returns a boolean indicating if the operation was successfull or not
	 */
	$bdd = DataBase::getInstance();
	$request = file_get_contents("model/sql/user/createCheck.sql");

	$prep = $bdd->prepare($request);
	$prep->bindParam(":login", $login, PDO::PARAM_STR);
	$prep->bindParam(":pseudo", $pseudo, PDO::PARAM_STR);
	$prep->bindParam(":email", $email, PDO::PARAM_STR);

	$prep->execute();
	$res = $prep->fetch();
	$prep->closeCursor();
	return $res;
}

function create_user(string $login, string $pseudo, string $email, string $password) {
	/*
	 *	Create a user with a complete set of data
	 *
	 *	Returns a boolean indicating if the operation was successfull or not
	 */
	$bdd = DataBase::getInstance();
	$request = file_get_contents("model/sql/user/create.sql");

	$prep = $bdd->prepare($request);
	$prep->bindParam(":login", $login, PDO::PARAM_STR);
	$prep->bindParam(":pseudo", $pseudo, PDO::PARAM_STR);
	$prep->bindParam(":email", $email, PDO::PARAM_STR);
	$prep->bindParam(":password", $password, PDO::PARAM_STR);

	try {
		$res = $prep->execute();
	}
	catch (PDOException $e) {
		return create_check_error(
			$login,
			$pseudo,
			$email
		);
	}
	return array();
}

function get_user_by_login(string $login) {
	/*
	 *	Retrieve pseudo, email and password of a user
	 */
	$bdd = DataBase::getInstance();
	$request = <<<EOT
SELECT
		id,
		pseudo,
		email,
		is_admin
FROM
		users
WHERE
		login = :login
;
EOT;

	$prep = $bdd->prepare($request);
	$prep->bindParam(":login", $login, PDO::PARAM_STR);

	$prep->execute();
	$res = $prep->fetch();
	$prep->closeCursor();
	return $res;
}

function get_user_by_pseudo(string $pseudo) {
	/*
	 *	Retrieve pseudo, email and password of a user
	 */
	$bdd = DataBase::getInstance();
	$request = <<<EOT
SELECT
		id,
		pseudo,
		email,
		is_admin
FROM
		users
WHERE
		pseudo = :pseudo
;
EOT;

	$prep = $bdd->prepare($request);
	$prep->bindParam(":pseudo", $pseudo, PDO::PARAM_STR);

	$prep->execute();
	$res = $prep->fetch();
	$prep->closeCursor();
	return $res;
}

function get_users() {
	/*
	 *	Retrieve pseudo, email and password of all users
	 *
	 *	With a big database, implement a function with a range
	 */
	$bdd = DataBase::getInstance();
	$request = <<<EOT
SELECT
		id,
		pseudo,
		email,
		is_admin
FROM
		users
;
EOT;

	$prep = $bdd->query($request);
	return $prep->fetchAll();
}

function delete_user_by_pseudo(string $pseudo, string $password) {
	/*
	 *	Delete a user with his pseudo and password
	 *
	 *	Should be use on user side
	 */
	$bdd = DataBase::getInstance();
	$request = <<<EOT
DELETE FROM
		users
WHERE
		pseudo = :pseudo AND password = :password AND IS_ADMIN = false
;
EOT;

	$prep = $bdd->prepare($request);
	$prep->bindParam(":pseudo", $pseudo, PDO::PARAM_STR);
	$prep->bindParam(":password", $password, PDO::PARAM_STR);

	return $prep->execute() !== false;
}

function delete_admin_by_pseudo(string $pseudo, string $password) {
	/*
	 *	Delete a user with his pseudo and password
	 *
	 *	Should be use on user side by admin
	 */
	$bdd = DataBase::getInstance();
	$request = <<<EOT
DELETE FROM
		users
WHERE
		pseudo = :pseudo AND password = :password AND IS_ADMIN = true
;
EOT;

	$prep = $bdd->prepare($request);
	$prep->bindParam(":pseudo", $pseudo, PDO::PARAM_STR);
	$prep->bindParam(":password", $password, PDO::PARAM_STR);

	return $prep->execute() !== false;
}

function delete_user(string $login) {
	/*
	 *	Delete a user with his login
	 *
	 *	Should be use on admin side only
	 */
	$bdd = DataBase::getInstance();
	$request = <<<EOT
DELETE FROM
		users
WHERE
		login = :login AND is_admin = false
;
EOT;

	$prep = $bdd->prepare($request);
	$prep->bindParam(":login", $login, PDO::PARAM_STR);

	return $prep->execute() !== false;
}

function delete_admin(string $login) {
	/*
	 *	Delete an admin with his login
	 *
	 *	Should be use on admin side only
	 */
	$bdd = DataBase::getInstance();
	$request = <<<EOT
DELETE FROM
		users
WHERE
		login = :login AND is_admin = false
;
EOT;

	$prep = $bdd->prepare($request);
	$prep->bindParam(":login", $login, PDO::PARAM_STR);

	return $prep->execute() !== false;
}
