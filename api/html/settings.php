<?php

$cwd = getcwd();

define("VIEWS_DIRECTORY", $cwd . "/views");
define("TEMPLATES_DIRECTORY", $cwd . "/templates");
define("BUNDLES_DIRECTORY", $cwd . "/bundles");

class DataBase {

	/*
	 *	This class is a singleton giving a connection to the db
	 */

	private static $_instance = null;
	private $_connector = null;

	private function __construct() {

		$host_bdd="172.18.0.4";
		$user_bdd="user";
		$user_pwd="password";

		try {
			$bdd = new PDO ("pgsql:host=".$host_bdd.";", $user_bdd, $user_pwd) or die(print_r($bdd->errorInfo()));
			$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$bdd->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		}
		catch(Exeption $e){
			die("Erreur!".$e->getMessage());
		}
		$this->_connector = $bdd;
	}

	public static function getInstance() {
		if (DataBase::$_instance == null) {
			DataBase::$_instance = new DataBase();
		}
		return Database::$_instance->_connector;
	}
}
