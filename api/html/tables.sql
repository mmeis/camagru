
DROP TABLE IF EXISTS comments;
DROP TABLE IF EXISTS pictures;
DROP TABLE IF EXISTS sessions;
DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS users
	(
		id			SERIAL PRIMARY KEY,
		login		varchar(128) NOT NULL UNIQUE,
		pseudo		varchar(40) NOT NULL UNIQUE,
		email		varchar(254) NOT NULL UNIQUE,
		password	varchar(128) NOT NULL,
		is_admin	boolean default FALSE
	)
;

CREATE TABLE IF NOT EXISTS sessions
	(
		id			SERIAL PRIMARY KEY,
		user_id		integer REFERENCES users ON DELETE CASCADE,
		is_connected boolean DEFAULT true
	)
;
ALTER TABLE sessions ADD COLUMN IF NOT EXISTS last_query TIMESTAMP;
ALTER TABLE sessions ADD COLUMN IF NOT EXISTS first_connected TIMESTAMP;
ALTER TABLE sessions ALTER COLUMN last_query SET DEFAULT NOW();
ALTER TABLE sessions ALTER COLUMN first_connected SET DEFAULT NOW();


CREATE TABLE IF NOT EXISTS pictures
	(
		id			SERIAL PRIMARY KEY,
		owner_id	integer REFERENCES users ON DELETE CASCADE UNIQUE,
		name		varchar(40),
		path		varchar(255) UNIQUE
	)
;
ALTER TABLE pictures ADD COLUMN IF NOT EXISTS created TIMESTAMP;
ALTER TABLE pictures ALTER COLUMN created SET DEFAULT NOW();

CREATE TABLE IF NOT EXISTS comments
	(
		id			SERIAL PRIMARY KEY,
		author_id	integer REFERENCES users ON DELETE CASCADE,
		picture_id	integer REFERENCES pictures ON DELETE CASCADE,
		text		varchar(512)
	)
;
ALTER TABLE comments ADD COLUMN IF NOT EXISTS created TIMESTAMP;
ALTER TABLE comments ALTER COLUMN created SET DEFAULT NOW();
