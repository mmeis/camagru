<?php

require_once("framework/view.php");
require_once("model/connect.php");

require_once("settings.php");

function hash_login(string $login) {
	return hash("sha512", "ç(!'i)$login");
}

function hash_password(string $password) {
	return hash("whirlpool", "cfgyuç$password");
}

class Connect extends ViewClass {

	function post(array $args) {
		if ($_POST["login"] && $_POST["password"]) {
			$res = [
				"status" => "ok",
				"errors" => []
			];

			$request = connect(
				hash_login($_POST["login"]),
				hash_password($_POST["password"])
			);
			if (!$request) {
				http_response_code(401);
				$res["status"] = "error";
				$res["errors"][] = "Could not connect with these credentials";
			}
			echo json_encode($res);
		}
		else {
			throw new Http400Error();
		}
	}

	function delete(array $args) {
		if ($_POST["session_id"] && is_numeric($_POST["session_id"])) {
			$res = [
				"status" => "ok",
				"errors" => [],
			];

			$request = disconnect(intval($_POST["session_id"]));
			var_dump($reauest);
			var_dump(!$reauest);
			if (!$request) {
				http_response_code(401);
				$res["status"] = "error";
				$res["errors"][] = "Could not disconnect this session";
			}
			echo json_encode($res);
		}
		else {
			throw new Http400Error();
		}
	}
}

?>
