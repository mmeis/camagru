<?php

require_once("settings.php");
require_once("framework/view.php");

require_once("model/install.php");

class Install extends ViewClass {
		function get(array $args) {
				if (install() !== false) {
						echo "<div>Installation successfull !</div>";
				}
				else {
						echo "<div>Installation failed :'( !</div>";
				}
		}
}

?>
