<?php

require_once("framework/view.php");
require_once("framework/renderer/renderer.php");

require_once("settings.php");
require_once("model/user.php");

/*
 * HELPERS
 */

function error_checker() {
	$errors = array();
	if (!$_POST["pseudo"]) {
		$errors[] = "Pseudo is not provided";
	}
	else {
		if (strlen($_POST["pseudo"]) < 8) {
			$errors[] = "Pseudo is less than 8 characters";
		}
		if (strlen($_POST["pseudo"]) > 20) {
			$errors[] = "Pseudo is less than 20 characters";
		}
		if (!ctype_alnum(str_replace(['-', '_'], '', $_POST["pseudo"]))) {
			$errors[] = "Pseudo contains forbidden characters ( not letters and not - or _ )";
		}
	}
	if (!$_POST["login"]) {
		$errors[] = "Login is not provided";
	}
	else {
		if (strlen($_POST["login"]) < 8) {
			$errors[] = "Login less than 8 characters";
		}
	}
	if (!$_POST["password"]) {
		$errors[] = "Password is not provided";
	}
	else {
		if (strlen($_POST["password"]) < 8) {
			$errors[] = "Password is less than 8 characters";
		}
		if (strlen($_POST["password"]) > 20) {
			$errors[] = "Password less than 20 characters";
		}
	}
	if (!$_POST["password_bis"]) {
		$errors[] = "Password bis is not provided";
	}
	else {
		if ($_POST["password_bis"] !== $_POST["password"]) {
			$errors[] = "Passwords does not match";
		}
	}
	if (!$_POST["email"]) {
		$errors[] = "Email is not provided";
	}
	else {
		if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
			$errors[] = "Email does not seems valid";
		}
	}
	return $errors;
}

function hash_login(string $login) {
	return hash("sha512", "ç(!'i)$login");
}

function hash_password(string $password) {
	return hash("whirlpool", "cfgyuç$password");
}

class User extends ViewClass {
	function get(array $args) {

		if ($args[0]) {
			$user = get_user_by_pseudo($args[0]);
			if ($user) {
				echo json_encode($user);
			}
			else {
				throw new Http404Error();
			}
		}
		else {
			echo json_encode(get_users());
		}
	}

	function post(array $args) {

		$res = [
			"errors" => null,
			"status" => "ok",
		];

		$res["errors"] = error_checker();
		if (count($res["errors"]) > 0) {
			$res["status"] = "error";
		}
		else {
			$creation = create_user(
				hash_login($_POST["login"]),
				$_POST["pseudo"],
				$_POST["email"],
				hash_password($_POST["password"]),
				false
			);

			if (count($creation) > 0) {
				$res["status"] = "errors";
				if ($creation["pseudo_exists"]) {
					$res["errors"][] = "This pseudo already exists";
				}
				if ($creation["login_exists"]) {
					$res["errors"][] = "This login already exists";
				}
				if ($creation["email_exists"]) {
					$res["errors"][] = "This email is already used";
				}
			}
		}
		if ($res["errors"]) {
			http_response_code(400);
		}
		echo json_encode($res) . "\n";
	}

}
