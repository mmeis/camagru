<?php

class HttpError extends Exception {
	protected $message = "An http error occured";
	protected $code = 500;
}

class Http400Error extends HttpError {
	protected $message = "Bad request";
	protected $code = 400;
}

class Http401Error extends HttpError {
	protected $message = "Unauthorized request";
	protected $code = 401;
}

class Http403Error extends HttpError {
	protected $message = "Forbidden request";
	protected $code = 403;
}

class Http404Error extends HttpError {
	protected $message = "Page not found";
	protected $code = 404;
}

class Http405Error extends HttpError {
	protected $message = "Method not allowed";
	protected $code = 405;
}
