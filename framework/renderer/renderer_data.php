<?php

require_once("settings.php");

function render_data(string $route, array $ctx, string $type="json") {
	$bundle_name = BUNDLE_DIRECTORY . $route;
	$source_name = TEMPLATES_DIRECTORY . $route;

	$render = null;

	if (file_exists($bundle_name) === false ||
		filemtime($source_name) > filemtime($bundle_name)) {
		if (file_exists($source_name) === false) {
			http_response_code(404);
			echo $file_name . " not found";
			die();
		}
		$render = bundler($source_name);
		file_put_contents(
			$bundle_name,
			$render
		);
	}
	else {
		$render = file_get_contents($bundle_name);
	}
	echo $render;
}
