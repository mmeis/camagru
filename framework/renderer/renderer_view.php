<?php

require_once("settings.php");
require_once("framework/errors/errors.php");

function bundler($file_name) {

	if (file_exists($file_name)) {
		$file = file_get_contents($file_name);

		if ($file === false) {
			throw new Http404Error();
		}
		$file = preg_replace_callback(
			"|([ \t]*)<include \"(.*)\">|",
			function ($matches) {
				/*
				 * Return the sub bundle re indented.
				 * Keep a sane readable bundle
				 */
				$bundler = "";
				$unindented_bundler = array_filter(explode(
					PHP_EOL,
					bundler(TEMPLATES_DIRECTORY . "/" . $matches[2])
				));
				foreach ($unindented_bundler as $line) {
					$bundler .= $matches[1] . $line . PHP_EOL;
				}
				return $bundler;
			},
			$file
		);
		return $file;

	}
	else {
		throw new Http404Error();
	}
}

function render(string $template, array $ctx) {
	$bundle_name = BUNDLES_DIRECTORY . $template;
	$source_name = TEMPLATES_DIRECTORY . $template;
	$render = null;

	if (file_exists(BUNDLES_DIRECTORY) === false) {
		mkdir(BUNDLES_DIRECTORY);
	}
	if (file_exists($bundle_name) === false ||
		filemtime($source_name) > filemtime($bundle_name)) {
		if (file_exists($source_name) === false) {
			throw new Http404Error();
		}
		$render = bundler($source_name);
		file_put_contents(
			$bundle_name,
			$render
		);
	}
	else {
		$render = file_get_contents($bundle_name);
	}
	echo $render;
}
