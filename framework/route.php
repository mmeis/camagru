<?php

require_once("settings.php");
require_once("framework/renderer/renderer.php");
require_once("framework/errors/errors.php");

class Route {

	private $_routes = [];

	function register(array $routes) {
		$this->_routes = array_merge($this->_routes, $routes);
	}

	private function call_view(string $view, array $args) {
		/*
		 *	Call the required view with http verb required, like
		 *	get, post, put, patch, delete
		 *
		 *	@throw http405Error, http404Error
		 */
		if (!@include_once(VIEWS_DIRECTORY . "/" . $view . ".php")) {
			throw new Http404Error();
		}
		try {

			$class_name = ucfirst($view);
			$view_instance = new $class_name();
		}
		catch (Exception $e) {
			throw new Http404Error();
		}

		$method = strtolower($_SERVER["REQUEST_METHOD"]);
		$view_instance->$method($args);

	}

	private function manage_sub_routeur($sub_routeur, $url_matches, $initial_args) {
		$INDEX_CURRENT_SEARCH = 1;
		$INDEX_FIRST_ARG = 2;
		$INDEX_NEXT_SEARCH = count($url_matches) - 1;
		$INDEX_LAST_ARG = $INDEX_NEXT_SEARCH - 1;

		$sub_routeur->process(
			$url_matches[$INDEX_NEXT_SEARCH],
			array_merge(
				$initial_args,
				array_slice($url_matches, $INDEX_FIRST_ARG, max(0, $INDEX_LAST_ARG - $INDEX_FIRST_ARG))
			)
		);
	}

	private function preg_iter_routes(string $route, array $args) {
		/*
		 *	Iter on routes and preg_match them to retrieve arguments required
		 *	by the route itself.
		 *	If the route link to an other router, delegate, otherwise call
		 *	the view
		 *
		 */
		foreach ($this->_routes as $key => $value) {
			$matches = [];

			if (preg_match($key, $route, $matches)) {
				if ($value instanceof Route) {
					$this->manage_sub_routeur($value, $matches, $args);
				}
				else {
					array_shift($matches);
					$args = array_merge($args, $matches);
					$this->call_view($value, $args);
				}
				return ;
			}
		}
		throw new Http404Error();
	}

	function process(string $route, array $args = []) {

		if (substr($route, -3, 3) === ".js" ||
			substr($route, -4, 4) === ".css") {
			render($route, $args);
			return ;
		}

		try {
			$this->preg_iter_routes($route, $args);
		}
		catch (HttpError $e) {
			http_response_code($e->getCode());
			echo $e->getMessage();
			die();
		}
	}
}
