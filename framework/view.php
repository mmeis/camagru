<?

require("framework/errors/errors.php");

class ViewClass {
	public function get(array $args) {
		throw new Http405Error();
	}

	public function post(array $args) {
		throw new Http405Error();
	}

	public function put(array $args) {
		throw new Http405Error();
	}

	public function patch(array $args) {
		throw new Http405Error();
	}

	public function delete(array $args) {
		throw new Http405Error();
	}

}
