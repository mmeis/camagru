<?php

require_once("framework/route.php");

$routes = Array(
	"|^/$|" => "home",
	"|^/picture$|" => "picture",
);

$router = new Route();
$router->register($routes);

$router->process($_SERVER["REQUEST_URI"]);

?>
