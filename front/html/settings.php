<?php

$cwd = getcwd();

define("VIEWS_DIRECTORY", $cwd . "/views");
define("TEMPLATES_DIRECTORY", $cwd . "/templates");
define("BUNDLES_DIRECTORY", $cwd . "/bundles");
