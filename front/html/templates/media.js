
(function() {
	/*
	 *	Polyfill the mediadevices to old browser
	 */
	if (navigator.mediaDevices === undefined) {
		navigator.mediaDevices = {};
	}

	if (navigator.mediaDevices.getUserMedia === undefined) {
		navigator.mediaDevices.getUserMedia = function(constraints) {

			var getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

			if (!getUserMedia) {
				return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
			}

			return new Promise(function(resolve, reject) {
				getUserMedia.call(navigator, constraints, resolve, reject);
			});
		}
	}
})();

class Camagru {
	constructor() {

		this.videoElement = document.querySelector("#camagru");
		this.canvasElement = document.createElement("canvas");
		this.pictureElement = document.querySelector("#take-pic");

		this.pictureElement.addEventListener(
			"click",
			this.takePicture.bind(this)
		);
		/*
		 *	Get the video stream from camera
		 */
		navigator.mediaDevices.getUserMedia({video: {facingMode: "user"} })
			.then(stream => {
				console.log("[Camagru] Camera stream opened");
				this.stream = stream;
				this.videoElement.srcObject = stream;

				console.log(this.videoElement);
				this.canvasElement.width = this.videoElement.offsetWidth;
				this.canvasElement.height = this.videoElement.offsetHeight;

				this.videoElement.parentNode.insertBefore(
					this.canvasElement,
					this.pictureElement
				);
				this.canvasElement.classList.add("hide");

			})
			.catch(err => {
				console.log("[Camagru Warn] The camera is not accessible", err);
			});
	}

	takePicture() {
		const v = {
			x: this.canvasElement.width,
			y: this.canvasElement.height
		};
		this.canvasElement.getContext("2d").drawImage(
			this.videoElement,
			0, 0,
			v.x, v.y
		);
		this.toggleState();
	}

	toggleState() {
		this.canvasElement.classList.toggle("hide");
		this.videoElement.classList.toggle("hide");
	}
}

let c = new Camagru();
