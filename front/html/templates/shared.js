
"use strict";

function shared() {
	function selfNavigate() {
		/*
		 *	This function manage toggling show / hide navigation menu
		 */
		var navTrigger = document.querySelector("#nav-trigger");
		var navigation = document.querySelector("#navigation");

		navTrigger.addEventListener("click", () => navigation.classList.toggle("hide") );
		navigation.addEventListener("click", () => navigation.classList.toggle("hide") );
	}
	selfNavigate();

	var popup = {
		overlayId: "popup-overlay",
		stopPropagation: function (ev) {
			ev.stopPropagation();
		},
		escapeEvent: function (ev) {
			var isEscape = false;

			if ("key" in ev) {
				isEscape = (ev.key == "Escape" || ev.key == "Esc");
			}
			else {
				isEscape = (ev.keyCode == 27);
			}
			if (isEscape) {
				popup.off();
				ev.stopPropagation();
			}
		},
		on: function(popElement) {
			const overlay = document.createElement("DIV");

			// Remove the popup when click out / press esc
			overlay.addEventListener("click", this.off);
			popElement.addEventListener("click", this.stopPropagation);
			popElement.addEventListener("keypress", this.escapeEvent);

			overlay.classList.add("popup-overlay");
			overlay.id = popup.overlayId;

			// Be sure that the popup is visible
			popElement.style.display = "block";
			overlay.appendChild(popElement.parentElement.removeChild(popElement));

			document.body.appendChild(overlay);
		},
		off: function() {
			const overlay = document.getElementById(popup.overlayId);
			const formBase = document.querySelector("#form-base");

			if (overlay) {
				const popElement = formBase.appendChild(overlay.removeChild(overlay.firstChild));
				popElement.style.display = "none";

				// Living in a clean world
				popElement.removeEventListener("click", this.stopPropagation);
				popElement.removeEventListener("keypress", this.escapeEvent);
				overlay.parentElement.removeChild(overlay);
			}
			else {
				console.debug("popup off without popup");
			}
		},
	}

	var dataValidator = {
		/*
		 *	Simple functions validating data format
		 *
		 *	Returns an array containing strings describing errors for users
		 *	This is not meant to replace server data validation
		 */
		pseudo: function(pseudo) {
			const pseudoMaxLength = 36;
			const pseudoMinLength = 6;
			var regexValidator = /^[a-z0-9_\-]+$/i

			let errors = [];

			if (pseudo.length < pseudoMinLength) {
				errors.push("Pseudo must have at least " + pseudoMinLength + " characters");
			}
			else if (pseudo.length > pseudoMaxLength) {
				errors.push("Pseudo must have at most " + pseudoMaxLength + " characters");
			}
			if (!pseudo.match(regexValidator)) {
				errors.push("Pseudo must contain only hexadecimal characters, - or _");
			}

			return errors;
		},
		password: function(password, mirror=null) {
			const passwordMinLength = 8;

			let errors = [];

			if (password.length < passwordMinLength) {
				errors.push("Password must have at least " + passwordMinLength + " characters");
			}
			if (mirror !== null && password !== mirror) {
				errors.push("Passwords does not match");
			}
			return errors;
		},
		email: function(email) {

			var emailRegex = /(.+)@(.+){2,}\.(.+){2,}/;
			var errors = [];

			if (emailRegex.test(email) === false) {
				errors.push("The email does not seem to be valid");
			}
			return errors;
		}
	};

	class User {

		constructor() {
			this._user = null;
		}
	}
	var userInstance = new User();

	function displayTempMessage(errors, node, color="red", classes="", elem="div") {
		/*
		 *	Add temporary mesages on the node
		 */
		const MESS_CLASS = "mesage-tmp-node";

		classes = classes ? classes + MESS_CLASS : MESS_CLASS;

		for (let error of errors) {
			let errNode = document.createElement(elem);

			errNode.appendChild(document.createTextNode(error));
			errNode.classList.add(classes);
			errNode.style.color = color;
			errNode.style.transition = "color 1s, line-height 1s";
			node.appendChild(errNode);

			setTimeout(() => {
				errNode.style.color = "transparent";
				errNode.style.lineHeight = "0";
				setTimeout(() => node.removeChild(errNode), 1500);
			},4000);
		}
	}

	class Session {

		constructor() {
			this.signedIn = false;

			this.updateSessionMenu();
			const signinElement = document.querySelector("#signin-menu");
			const signupElement = document.querySelector("#signup-menu");
			const logoutElement = document.querySelector("#logout-menu");

			signinElement.addEventListener(
					"click",
					() => popup.on(document.querySelector("#signin"))
			);
			signupElement.addEventListener(
					"click",
					() => popup.on(document.querySelector("#signup"))
			);
			logoutElement.addEventListener(
					"click",
					this.logout.bind(this)
			);
		}

		signin() {
			const pseudoInput = document.querySelector("#signin-pseudo");
			const passwordInput = document.querySelector("#signin-password");

			const errorsPseudo = dataValidator.pseudo(pseudoInput.value);
			const errorsPassword = dataValidator.password(passwordInput.value);

			if (errorsPseudo.length > 0) {
				console.warn(errorsPseudo);
				displayTempMessage(errorsPseudo, pseudoInput.parentElement);
			}
			if (errorsPassword.length > 0) {
				console.warn(errorsPassword);
				displayTempMessage(errorsPassword, passwordInput.parentElement);
			}

			if (errorsPassword.length == 0 && errorsPseudo.length == 0) {
				console.debug("Signin server TODO");
				this.signedIn = true;
				this.updateSessionMenu();
				popup.off();
			}
		}

		signup() {
			const pseudoInput = document.querySelector("#signup-pseudo");
			const passwordInput = document.querySelector("#signup-password");
			const passwordInputBis = document.querySelector("#signup-password-bis");
			const emailInput = document.querySelector("#signup-email");

			const errorsPseudo = dataValidator.pseudo(pseudoInput.value);
			const errorsPassword = dataValidator.password(passwordInput.value, passwordInput.value);
			const errorsMail = dataValidator.email(emailInput.value);

			if (errorsPseudo.length > 0) {
				console.warn(errorsPseudo);
				displayTempMessage(errorsPseudo, pseudoInput.parentElement);
			}
			if (errorsPassword.length > 0) {
				console.warn(errorsPassword);
				displayTempMessage(errorsPassword, passwordInput.parentElement);
			}
			if (errorsMail.length > 0) {
				console.warn(errorsPassword);
				displayTempMessage(errorsMail, emailInput.parentElement);
			}

			if (errorsPassword.length == 0 && errorsPseudo.length == 0
				&& errorsMail == 0) {
				console.debug("Signup server TODO");
				this.signedIn = true;
				this.updateSessionMenu();
			}
		}

		logout() {
			console.debug("Logout server TODO");
			this.signedIn = false;
			this.updateSessionMenu();
		}

		updateSessionMenu() {
			const signinElement = document.querySelector("#signin-menu");
			const signupElement = document.querySelector("#signup-menu");
			const logoutElement = document.querySelector("#logout-menu");

			if (this.signedIn) {
				signinElement.classList.add("hide");
				signupElement.classList.add("hide");
				logoutElement.classList.remove("hide");
			}
			else {
				signinElement.classList.remove("hide");
				signupElement.classList.remove("hide");
				logoutElement.classList.add("hide");
			}
		}
	}
	var sessionInstance = new Session();

	document.querySelector("#signin-form").addEventListener("submit", ev => {
		ev.preventDefault();
		console.log("Signin in");
		sessionInstance.signin();
	});
	document.querySelector("#signup-form").addEventListener("submit", ev => {
		ev.preventDefault();
		sessionInstance.signup();
	});
	<include "media.js">
}
shared();
