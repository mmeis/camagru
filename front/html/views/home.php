<?php

require_once("framework/view.php");
require_once("framework/renderer/renderer.php");

require_once("settings.php");

class Home extends ViewClass {
	function get(array $args) {
		$template = "/home.html";
		render($template, $args);
	}
}
