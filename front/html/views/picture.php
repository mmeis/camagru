<?php

require_once("framework/view.php");
require_once("framework/renderer/renderer.php");

require_once("settings.php");

class Picture extends ViewClass {
	function get(array $args) {
		$template = "/picture.html";
		render($template, $args);
	}
}
