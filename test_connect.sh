
curl -X POST "http://localhost:8000/api/v1/connect" \
	--header "Content-Type: application/json" \
	--data '{"login": "anyLogin", "password": "anyPassword"}' \
	-o - -s -w "\n%{http_code}\n"
echo

curl -X POST "http://localhost:8000/api/v1/connect" \
	--header "Content-Type: application/json" \
	--data '{"login": "notLogin", "password": "anyPassword"}' \
	-o - -s -w "\n%{http_code}\n"
echo

curl -X POST "http://localhost:8000/api/v1/connect" \
	--header "Content-Type: application/json" \
	--data '{"lgin": "anyLogin", "password": "anyPassword"}' \
	-o - -s -w "\n%{http_code}\n"
echo

curl -X DELETE "http://localhost:8000/api/v1/connect" \
	--header "Content-Type: application/json" \
	--data '{"session_id": 1}' \
	-o - -s -w "\n%{http_code}\n"
echo
