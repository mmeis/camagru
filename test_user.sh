

# Empty listing
curl -X GET "http://localhost:8000/api/v1/user/" -o - -s -w "\n%{http_code}\n"
echo

# Create an user
curl -X POST "http://localhost:8000/api/v1/user/" \
	-H "Content-Type:  application/json" \
	--data '{"login": "anyLogin", "pseudo": "anyPseudo", "password": "anyPassword", "password_bis": "anyPassword", "email": "mmeisson@student.42.fr"}' \
	-o - -s -w "%{http_code}\n"
echo

# try to recreate the user
curl -X POST "http://localhost:8000/api/v1/user/" \
	-H "Content-Type:  application/json" \
	--data '{"login": "anyLogin", "pseudo": "anyPseudo", "password": "anyPassword", "password_bis": "anyPassword", "email": "mmeisson@student.42.fr"}' \
	-o - -s -w "%{http_code}\n"
echo

# try to create an user with mail used
curl -X POST "http://localhost:8000/api/v1/user/" \
	-H "Content-Type:  application/json" \
	--data '{"login": "any1Login", "pseudo": "any1Pseudo", "password": "any1Password", "password_bis": "any1Password", "email": "mmeisson@student.42.fr"}' \
	-o - -s -w "%{http_code}\n"
echo

# Create an other user
curl -X POST "http://localhost:8000/api/v1/user/" \
	-H "Content-Type:  application/json" \
	--data '{"login": "any1Login", "pseudo": "any1Pseudo", "password": "any1Password", "password_bis": "any1Password", "email": "n.u.maxime@hotmail.fr"}' \
	-o - -s -w "%{http_code}\n"
echo

# Retrieve a valid user
curl -X GET "http://localhost:8000/api/v1/user/anyPseudo" -o - -s -w "\n%{http_code}\n"
echo

# Retrieve a not valid user
curl -X GET "http://localhost:8000/api/v1/user/notExists" -o - -s -w "\n%{http_code}\n"
echo

# Retrieve all users
curl -X GET "http://localhost:8000/api/v1/user/" -o - -s -w "\n%{http_code}\n"
echo
